<?php

/**
 * @file
 * Contains ansible_playbook.page.inc.
 *
 * Page callback for Ansible playbook entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ansible playbook templates.
 *
 * Default template: ansible_playbook.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ansible_playbook(array &$variables) {
  // Fetch AnsiblePlaybook Entity Object.
  $ansible_playbook = $variables['elements']['#ansible_playbook'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
