This module requires the following packages:

"asm/php-ansible": "v1.1.0"
"symfony/yaml": "3.4.x-dev"

They should be satisfied using composer install on module root.

On drupal 8 you can add dependencies on root composer json using wikimedia/composer-merge-plugin

Just add the path to this module's composer.json to root's composer.json under merge-plugin

    "extra": {
        "merge-plugin": {
            "include": [
                ...
            ],
            "require": [
                ...
                "modules/ansible_manager/composer.json"
            ],