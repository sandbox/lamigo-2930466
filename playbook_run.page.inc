<?php

/**
 * @file
 * Contains playbook_run.page.inc.
 *
 * Page callback for Playbook run entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Playbook run templates.
 *
 * Default template: playbook_run.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_playbook_run(array &$variables) {
  // Fetch PlaybookRun Entity Object.
  $playbook_run = $variables['elements']['#playbook_run'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
