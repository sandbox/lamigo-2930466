<?php

/**
 * @file
 * Contains ansible_host.page.inc.
 *
 * Page callback for Ansible host entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ansible host templates.
 *
 * Default template: ansible_host.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ansible_host(array &$variables) {
  // Fetch AnsibleHost Entity Object.
  $ansible_host = $variables['elements']['#ansible_host'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
