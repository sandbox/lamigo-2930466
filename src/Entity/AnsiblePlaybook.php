<?php

namespace Drupal\ansiblemanager\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Ansible playbook entity.
 *
 * @ingroup ansiblemanager
 *
 * @ContentEntityType(
 *   id = "ansible_playbook",
 *   label = @Translation("Ansible playbook"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ansiblemanager\AnsiblePlaybookListBuilder",
 *     "views_data" = "Drupal\ansiblemanager\Entity\AnsiblePlaybookViewsData",
 *     "translation" = "Drupal\ansiblemanager\AnsiblePlaybookTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\ansiblemanager\Form\AnsiblePlaybookForm",
 *       "add" = "Drupal\ansiblemanager\Form\AnsiblePlaybookForm",
 *       "edit" = "Drupal\ansiblemanager\Form\AnsiblePlaybookForm",
 *       "delete" = "Drupal\ansiblemanager\Form\AnsiblePlaybookDeleteForm",
 *     },
 *     "access" = "Drupal\ansiblemanager\AnsiblePlaybookAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\ansiblemanager\AnsiblePlaybookHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ansible_playbook",
 *   data_table = "ansible_playbook_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer ansible playbook entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ansible_playbook/{ansible_playbook}",
 *     "add-form" = "/admin/structure/ansible_playbook/add",
 *     "edit-form" = "/admin/structure/ansible_playbook/{ansible_playbook}/edit",
 *     "delete-form" = "/admin/structure/ansible_playbook/{ansible_playbook}/delete",
 *     "collection" = "/admin/structure/ansible_playbook",
 *   },
 *   field_ui_base_route = "ansible_playbook.settings"
 * )
 */
class AnsiblePlaybook extends ContentEntityBase implements AnsiblePlaybookInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

    /**
     * {@inheritdoc}
     */
    public function getDescription() {
        return $this->get('description')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description) {
        $this->set('description', $description);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlaybook() {
        return $this->get('playbook')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlaybook($playbook) {
        $this->set('playbook', $playbook);
        return $this;
    }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of the author of Ansible playbook.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Ansible playbook.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['description'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Description'))
          ->setDescription(t('The description of the Ansible playbook.'))
          ->setSettings([
              'max_length' => 1024,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string_textarea',
              'weight' => -3,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textarea',
              'weight' => -3,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['playbook'] = BaseFieldDefinition::create('string_long')
          ->setLabel(t('Playbook'))
          ->setDescription(t('The content of the Ansible playbook.'))
          ->setSettings([
              'max_length' => 1024,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string_textarea',
              'weight' => -3,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textarea',
              'weight' => -3,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active status'))
      ->setDescription(t('A boolean indicating whether the Ansible playbook is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the playbook was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the playbook was last edited.'));

    return $fields;
  }
}
