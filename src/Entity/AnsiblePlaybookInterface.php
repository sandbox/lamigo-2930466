<?php

namespace Drupal\ansiblemanager\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ansible playbook entities.
 *
 * @ingroup ansiblemanager
 */
interface AnsiblePlaybookInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Ansible playbook name.
   *
   * @return string
   *   Name of the Ansible playbook.
   */
  public function getName();

  /**
   * Sets the Ansible playbook name.
   *
   * @param string $name
   *   The Ansible playbook name.
   *
   * @return \Drupal\ansiblemanager\Entity\AnsiblePlaybookInterface
   *   The called Ansible playbook entity.
   */
  public function setName($name);

  /**
   * Gets the Ansible playbook creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ansible playbook.
   */
  public function getCreatedTime();

  /**
   * Sets the Ansible playbook creation timestamp.
   *
   * @param int $timestamp
   *   The Ansible playbook creation timestamp.
   *
   * @return \Drupal\ansiblemanager\Entity\AnsiblePlaybookInterface
   *   The called Ansible playbook entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ansible playbook published status indicator.
   *
   * Unpublished Ansible playbook are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ansible playbook is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ansible playbook.
   *
   * @param bool $published
   *   TRUE to set this Ansible playbook to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\ansiblemanager\Entity\AnsiblePlaybookInterface
   *   The called Ansible playbook entity.
   */
  public function setPublished($published);

}
