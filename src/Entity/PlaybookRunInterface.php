<?php

namespace Drupal\ansiblemanager\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Playbook run entities.
 *
 * @ingroup ansiblemanager
 */
interface PlaybookRunInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Playbook run name.
   *
   * @return string
   *   Name of the Playbook run.
   */
  public function getName();

  /**
   * Sets the Playbook run name.
   *
   * @param string $name
   *   The Playbook run name.
   *
   * @return \Drupal\ansiblemanager\Entity\PlaybookRunInterface
   *   The called Playbook run entity.
   */
  public function setName($name);

  /**
   * Gets the Playbook run creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Playbook run.
   */
  public function getCreatedTime();

  /**
   * Sets the Playbook run creation timestamp.
   *
   * @param int $timestamp
   *   The Playbook run creation timestamp.
   *
   * @return \Drupal\ansiblemanager\Entity\PlaybookRunInterface
   *   The called Playbook run entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Playbook run published status indicator.
   *
   * Unpublished Playbook run are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Playbook run is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Playbook run.
   *
   * @param bool $published
   *   TRUE to set this Playbook run to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\ansiblemanager\Entity\PlaybookRunInterface
   *   The called Playbook run entity.
   */
  public function setPublished($published);

}
