<?php

namespace Drupal\ansiblemanager\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Ansible host entity.
 *
 * @ingroup ansiblemanager
 *
 * @ContentEntityType(
 *   id = "ansible_host",
 *   label = @Translation("Ansible host"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ansiblemanager\AnsibleHostListBuilder",
 *     "views_data" = "Drupal\ansiblemanager\Entity\AnsibleHostViewsData",
 *     "translation" = "Drupal\ansiblemanager\AnsibleHostTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\ansiblemanager\Form\AnsibleHostForm",
 *       "add" = "Drupal\ansiblemanager\Form\AnsibleHostForm",
 *       "edit" = "Drupal\ansiblemanager\Form\AnsibleHostForm",
 *       "delete" = "Drupal\ansiblemanager\Form\AnsibleHostDeleteForm",
 *     },
 *     "access" = "Drupal\ansiblemanager\AnsibleHostAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\ansiblemanager\AnsibleHostHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ansible_host",
 *   data_table = "ansible_host_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer ansible host entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ansible_host/{ansible_host}",
 *     "add-form" = "/admin/structure/ansible_host/add",
 *     "edit-form" = "/admin/structure/ansible_host/{ansible_host}/edit",
 *     "delete-form" = "/admin/structure/ansible_host/{ansible_host}/delete",
 *     "collection" = "/admin/structure/ansible_host",
 *   },
 *   field_ui_base_route = "ansible_host.settings"
 * )
 */
class AnsibleHost extends ContentEntityBase implements AnsibleHostInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

    /**
     * {@inheritdoc}
     */
    public function getIP() {
        return $this->get('ip')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setIP($ip) {
        $this->set('ip', $ip);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSshUsername() {
        return $this->get('ssh_username')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setSshUsername($username) {
        $this->set('ssh_username', $username);
        return $this;
    }

    public function getSshPassword() {
        return $this->get('ssh_password')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setSshPassword($username) {
        $this->set('ssh_password', $username);
        return $this;
    }

    /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Ansible host.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Name given to ansible host.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'Inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['ip'] = BaseFieldDefinition::create('string')
          ->setLabel(t('IP'))
          ->setDescription(t('IP the given host.'))
          ->setSettings([
              'max_length' => 50,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'Inline',
              'type' => 'string',
              'weight' => -3,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => -3,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['ssh_username'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Username'))
          ->setDescription(t('User name used to ssh into this host.'))
          ->setSettings([
              'max_length' => 50,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'Inline',
              'type' => 'string',
              'weight' => -2,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => -2,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['ssh_password'] = BaseFieldDefinition::create('password')
          ->setLabel(t('Password'))
          ->setDescription(t('Password used to ssh into this host.'))
          ->setSettings([
              'max_length' => 50,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'Inline',
              'type' => 'password',
              'weight' => -1,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => -1,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active status'))
      ->setDescription(t('A boolean indicating whether the Ansible host is active or not.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when host was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when host was last edited.'));

    return $fields;
  }

}
