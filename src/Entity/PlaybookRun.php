<?php

namespace Drupal\ansiblemanager\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Playbook run entity.
 *
 * @ingroup ansiblemanager
 *
 * @ContentEntityType(
 *   id = "playbook_run",
 *   label = @Translation("Playbook run"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ansiblemanager\PlaybookRunListBuilder",
 *     "views_data" = "Drupal\ansiblemanager\Entity\PlaybookRunViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\ansiblemanager\Form\PlaybookRunForm",
 *       "add" = "Drupal\ansiblemanager\Form\PlaybookRunForm",
 *       "edit" = "Drupal\ansiblemanager\Form\PlaybookRunForm",
 *       "delete" = "Drupal\ansiblemanager\Form\PlaybookRunDeleteForm",
 *     },
 *     "access" = "Drupal\ansiblemanager\PlaybookRunAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\ansiblemanager\PlaybookRunHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "playbook_run",
 *   admin_permission = "administer playbook run entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/playbook_run/{playbook_run}",
 *     "add-form" = "/admin/structure/playbook_run/add",
 *     "edit-form" = "/admin/structure/playbook_run/{playbook_run}/edit",
 *     "delete-form" = "/admin/structure/playbook_run/{playbook_run}/delete",
 *     "collection" = "/admin/structure/playbook_run",
 *   },
 *   field_ui_base_route = "playbook_run.settings"
 * )
 */
class PlaybookRun extends ContentEntityBase implements PlaybookRunInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of user who run the playbook.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Description of the Playbook run.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['host_id'] = BaseFieldDefinition::create('entity_reference')
          ->setLabel(t('Authored by'))
          ->setDescription(t('The host ID in which the playbook is run.'))
          ->setRevisionable(TRUE)
          ->setSetting('target_type', 'ansible_host')
          ->setSetting('handler', 'default')
          ->setTranslatable(TRUE)
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'entity_reference_autocomplete',
              'weight' => 5,
              'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => '60',
                  'autocomplete_type' => 'tags',
                  'placeholder' => '',
              ],
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['playbook_id'] = BaseFieldDefinition::create('entity_reference')
          ->setLabel(t('Playbook'))
          ->setDescription(t('The playbook to run.'))
          ->setRevisionable(TRUE)
          ->setSetting('target_type', 'ansible_playbook')
          ->setSetting('handler', 'default')
          ->setTranslatable(TRUE)
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'entity_reference_autocomplete',
              'weight' => 5,
              'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => '60',
                  'autocomplete_type' => 'tags',
                  'placeholder' => '',
              ],
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['variables'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Variables'))
          ->setDescription(t('Variables to be added to the Playbook run in the form label=value.'))
          ->setCardinality(CARDINALITY_UNLIMITED)
          ->setSettings([
              'max_length' => 255,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => -4,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => -4,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);



      $fields['finished'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Playbook run has been finished.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the run was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
