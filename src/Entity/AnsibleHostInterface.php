<?php

namespace Drupal\ansiblemanager\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ansible host entities.
 *
 * @ingroup ansiblemanager
 */
interface AnsibleHostInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Ansible host name.
   *
   * @return string
   *   Name of the Ansible host.
   */
  public function getName();

  /**
   * Sets the Ansible host name.
   *
   * @param string $name
   *   The Ansible host name.
   *
   * @return \Drupal\ansiblemanager\Entity\AnsibleHostInterface
   *   The called Ansible host entity.
   */
  public function setName($name);

  /**
   * Gets the Ansible host creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ansible host.
   */
  public function getCreatedTime();

  /**
   * Sets the Ansible host creation timestamp.
   *
   * @param int $timestamp
   *   The Ansible host creation timestamp.
   *
   * @return \Drupal\ansiblemanager\Entity\AnsibleHostInterface
   *   The called Ansible host entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ansible host published status indicator.
   *
   * Unpublished Ansible host are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ansible host is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ansible host.
   *
   * @param bool $published
   *   TRUE to set this Ansible host to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\ansiblemanager\Entity\AnsibleHostInterface
   *   The called Ansible host entity.
   */
  public function setPublished($published);

}
