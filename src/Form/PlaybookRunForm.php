<?php

namespace Drupal\ansiblemanager\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Playbook run edit forms.
 *
 * @ingroup ansiblemanager
 */
class PlaybookRunForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\ansiblemanager\Entity\PlaybookRun */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Playbook run.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Playbook run.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.playbook_run.canonical', ['playbook_run' => $entity->id()]);
  }

}
