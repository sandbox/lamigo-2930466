<?php

namespace Drupal\ansiblemanager\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Playbook run entities.
 *
 * @ingroup ansiblemanager
 */
class PlaybookRunDeleteForm extends ContentEntityDeleteForm {


}
