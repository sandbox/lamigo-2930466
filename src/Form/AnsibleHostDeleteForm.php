<?php

namespace Drupal\ansiblemanager\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ansible host entities.
 *
 * @ingroup ansiblemanager
 */
class AnsibleHostDeleteForm extends ContentEntityDeleteForm {


}
