<?php

namespace Drupal\ansiblemanager\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ansible playbook entities.
 *
 * @ingroup ansiblemanager
 */
class AnsiblePlaybookDeleteForm extends ContentEntityDeleteForm {


}
