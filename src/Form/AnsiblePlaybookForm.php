<?php

namespace Drupal\ansiblemanager\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Ansible playbook edit forms.
 *
 * @ingroup ansiblemanager
 */
class AnsiblePlaybookForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\ansiblemanager\Entity\AnsiblePlaybook */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Ansible playbook.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Ansible playbook.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.ansible_playbook.canonical', ['ansible_playbook' => $entity->id()]);
  }

}
