<?php

namespace Drupal\ansiblemanager;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Ansible playbook entities.
 *
 * @ingroup ansiblemanager
 */
class AnsiblePlaybookListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Ansible playbook ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\ansiblemanager\Entity\AnsiblePlaybook */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.ansible_playbook.edit_form', [
          'ansible_playbook' => $entity->id(),
        ]
      )
    );
    return $row + parent::buildRow($entity);
  }

}
