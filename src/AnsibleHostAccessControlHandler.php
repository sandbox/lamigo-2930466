<?php

namespace Drupal\ansiblemanager;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Ansible host entity.
 *
 * @see \Drupal\ansiblemanager\Entity\AnsibleHost.
 */
class AnsibleHostAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ansiblemanager\Entity\AnsibleHostInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ansible host entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ansible host entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ansible host entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ansible host entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ansible host entities');
  }

}
