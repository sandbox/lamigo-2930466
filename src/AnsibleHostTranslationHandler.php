<?php

namespace Drupal\ansiblemanager;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for ansible_host.
 */
class AnsibleHostTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
