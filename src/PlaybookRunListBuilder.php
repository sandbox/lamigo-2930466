<?php

namespace Drupal\ansiblemanager;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Playbook run entities.
 *
 * @ingroup ansiblemanager
 */
class PlaybookRunListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Playbook run ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\ansiblemanager\Entity\PlaybookRun */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.playbook_run.edit_form', [
          'playbook_run' => $entity->id(),
        ]
      )
    );
    return $row + parent::buildRow($entity);
  }

}
